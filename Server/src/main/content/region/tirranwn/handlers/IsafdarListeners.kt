package content.region.tirranwn.handlers

import core.game.interaction.IntType
import core.game.interaction.InteractionListener
import core.game.world.map.Location

/**
 * File to be used for anything Isafdar related.
 * Handles the summoning/altar cave enter and exit in Isafdar.
 */
class IsafdarListeners : InteractionListener {

    val CAVE_ENTRANCE = 4006
    val CAVE_EXIT = 4007
    val outside = Location.create(2312, 3217, 0)
    val inside = Location.create(2314, 9624, 0)

    override fun defineListeners() {
        on(CAVE_ENTRANCE, IntType.SCENERY, "enter"){ player, node ->
            player.teleport(inside)
            return@on true
        }

        on(CAVE_EXIT, IntType.SCENERY, "exit"){ player, node ->
            player.teleport(outside)
            return@on true
        }
    }
}