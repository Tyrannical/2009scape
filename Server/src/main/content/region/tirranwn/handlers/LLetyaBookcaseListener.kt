package content.region.tirranwn.handlers

import config.Items
import config.Scenery
import core.api.addItem
import core.api.inInventory
import core.api.sendMessage
import core.game.interaction.IntType
import core.game.interaction.InteractionListener

/**
 * Listener for obtaining the Prifddinas history book.
 */
class LLetyaBookcaseListener : InteractionListener {

    val BOOKCASE = Scenery.BOOKCASE_8752
    val PRIFDDINAS_BOOK = Items.PRIFDDINAS_HISTORY_6073

    override fun defineListeners() {

        //The World Gate is only mentioned briefly during the Plague City storyline.
        //It is spoken of in the book Prifddinas' history.

        on(BOOKCASE, IntType.SCENERY, "search") { player, _ ->
            if (!inInventory(player, PRIFDDINAS_BOOK)) {
                sendMessage(player, "You search the bookshelves and find a book named 'Prifddinas History'")
                addItem(player, PRIFDDINAS_BOOK)
            } else {
                sendMessage(player, "You search the bookshelves...")
                sendMessage(player, "You don't find anything interesting.")
            }
            return@on true
        }
    }
}