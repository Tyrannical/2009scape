package content.minigame.vinesweeper.dialogue

import config.NPCs
import core.api.openDialogue
import core.game.dialogue.DialoguePlugin
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable

/**
 * Represents the Mrs Winkin dialogue plugin used for Vinesweeper mini-game.
 */
@Initializable
class MrsWinkinDialogue(player: Player? = null) : DialoguePlugin(player) {

    override fun open(vararg args: Any?): Boolean {
        npc = args[0] as NPC
        openDialogue(player!!, MrsWinkinDialogueFile(), npc)
        return true
    }
    override fun handle(interfaceId: Int, buttonId: Int): Boolean {
        return true
    }

    override fun getIds(): IntArray {
        return intArrayOf(NPCs.MRS_WINKIN_7132)
    }
}