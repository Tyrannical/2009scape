package content.quest.member.trollstronghold.dialogue

import core.api.setQuestStage
import core.game.dialogue.DialogueFile
import core.game.dialogue.FacialExpression
import core.game.dialogue.Topic
import core.tools.END_DIALOGUE

/**
 * Represents the Dad dialogue file used in Troll Stronghold quest.
 */
class DadDialogueFile(private val dialogueNum: Int = 0) : DialogueFile() {
    override fun handle(componentID: Int, buttonID: Int) {
        when(dialogueNum) {
            1 -> when (stage) {
                0 -> npcl(FacialExpression.OLD_HAPPY, "No human pass through arena without defeating Dad!").also {
                    stage = END_DIALOGUE
                    setQuestStage(player!!, "Troll Stronghold", 3)
                }
            }
            2 -> when (stage) {
                0 -> npcl(FacialExpression.OLD_NORMAL, "Tiny human brave. Dad squish!").also { stage++ }
                1 -> {
                    npc!!.attack(player).also {
                        npc!!.skills.lifepoints = npc!!.skills.maximumLifepoints
                        setQuestStage(player!!, "Troll Stronghold", 4)
                        stage = END_DIALOGUE
                    }
                }
            }
            3 -> when (stage) {
                0 -> npcl(FacialExpression.OLD_NORMAL, "Stop! You win. Not hurt Dad.").also { stage++ }
                1 -> showTopics(Topic(FacialExpression.FRIENDLY, "I'll be going now.", END_DIALOGUE), Topic(FacialExpression.ANGRY_WITH_SMILE, "I'm not done yet! Prepare to die!", 2))
                2 -> {
                    player!!.attack(npc).also {
                        setQuestStage(player!!, "Troll Stronghold", 5)
                        stage = END_DIALOGUE
                    }
                }
            }
        }
    }
}