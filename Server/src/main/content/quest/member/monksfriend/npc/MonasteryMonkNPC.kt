package content.quest.member.monksfriend.npc

import config.NPCs
import core.game.dialogue.DialogueFile
import core.game.dialogue.FacialExpression
import core.game.interaction.IntType
import core.game.interaction.InteractionListener
import core.game.node.entity.npc.NPC
import core.tools.END_DIALOGUE

/**
 * Represents the Monastery Monk dialogue file.
 */
class MonasteryMonkDialogue : DialogueFile() {
    override fun handle(interfaceId: Int, buttonId: Int) {
        var questStage = player!!.questRepository.getStage("Monk's Friend")
        if (questStage == 0) {
            when (stage) {
                0 -> npcl(FacialExpression.NEUTRAL, "Peace brother.").also { stage = END_DIALOGUE }
            }
        } else if (questStage < 100) {
            when (stage) {
                0 -> npcl(FacialExpression.FRIENDLY, "*yawn*").also { stage = END_DIALOGUE }
            }
        } else {
            when (stage) {
                0 -> npcl(FacialExpression.HAPPY, "Can't wait for the party!").also { stage = END_DIALOGUE }
            }
        }
    }
}

/**
 * Handles BrotherCedricListener to launch the dialogue
 */
class MonasteryMonkListener : InteractionListener {
    override fun defineListeners() {
        on(NPCs.MONK_281, IntType.NPC, "talk-to"){ player, _ ->
            player.dialogueInterpreter.open(MonasteryMonkDialogue(), NPC(NPCs.MONK_281))
            return@on true
        }
    }
}
