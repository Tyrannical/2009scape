package content.quest.member.bigchompybirdhunting.dialogue.bugs

import content.quest.member.bigchompybirdhunting.ChompyBird
import core.api.getAttribute
import core.api.getItemName
import core.api.setAttribute
import core.game.dialogue.DialogueFile
import core.game.dialogue.FacialExpression
import core.game.node.entity.player.Player
import core.game.node.entity.player.link.quest.Quest
import core.tools.END_DIALOGUE

/**
 * Represents the dialogue plugin used for the Bugs Chompy Bird.
 */
class BugsChompyBirdDialogue(val quest: Quest) : DialogueFile() {
  override fun handle(componentId: Int, buttonId: Int) {
    when (quest.getStage(player)) {
      in 0 until 20 -> npcl(FacialExpression.OLD_NORMAL, "You's better talk to Dad, him chasey sneaky da chompy.").also { stage = END_DIALOGUE }
      in 20 until 70 -> handleBellowsDialogue(player, buttonId)
      in 70 until 90 -> handleIngredientDialogue(player, buttonId)
    }
  }

  private fun handleBellowsDialogue(player: Player?, buttonId: Int) {
    when(stage) {
      0 -> playerl("Rantz said that you play with the fatsy toadies, what are they?").also { stage++ }
      1 -> npcl(FacialExpression.OLD_NORMAL, "Oh, we sometimes use da blower on da toadies but Dad don't let us get in da locked box no more. He he it was good fun making da toadies fat on da swamp gas.").also { stage = END_DIALOGUE }
    }
  }

  private fun handleIngredientDialogue(player: Player?, buttonId: Int) {
    val bugsIngredient = getAttribute(player!!, ChompyBird.ATTR_ING_BUGS, -1)
    when(stage) {
      0 -> npcl(FacialExpression.OLD_NORMAL, "Dad say's you's making da chompy for us! Slurp! Me's has to have ${getItemName(bugsIngredient)} wiv mine! Chompy is our favourite yummms!").also { stage = END_DIALOGUE }
    }
    setAttribute(player!!, ChompyBird.ATTR_BUGS_ASKED, true)
  }
}