package content.quest.member.bigchompybirdhunting.dialogue.bugs

import config.NPCs
import core.game.dialogue.DialoguePlugin
import core.game.node.entity.npc.NPC
import core.game.node.entity.player.Player
import core.plugin.Initializable

/**
 * Represents the dialogue plugin used for the Bugs.
 */
@Initializable
class BugsDialogue(player: Player? = null) : DialoguePlugin(player) {
  override fun getIds() : IntArray {
    return intArrayOf(NPCs.BUGS_1012)
  }

  override fun newInstance(player: Player?) : DialoguePlugin {
    return BugsDialogue(player)
  }

  override fun open(vararg args: Any?) : Boolean {
    npc = args[0] as NPC

    val chompyBird = player.questRepository.getQuest("Big Chompy Bird Hunting")
    val chompyStage = chompyBird.getStage(player)

    when (chompyStage) {
      in 0 until 100 -> loadFile(BugsChompyBirdDialogue(chompyBird))
    }

    player.dialogueInterpreter.handle(0,0)

    return true
  }

  override fun handle(componentId: Int, buttonId: Int) : Boolean {
    return true
  }
}