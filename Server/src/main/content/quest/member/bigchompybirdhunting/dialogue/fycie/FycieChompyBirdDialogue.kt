package content.quest.member.bigchompybirdhunting.dialogue.fycie

import content.quest.member.bigchompybirdhunting.ChompyBird
import core.api.getAttribute
import core.api.getItemName
import core.api.setAttribute
import core.game.dialogue.DialogueFile
import core.game.dialogue.FacialExpression
import core.game.node.entity.player.Player
import core.game.node.entity.player.link.quest.Quest
import core.tools.END_DIALOGUE

/**
 * Represents the dialogue file used for the Fycie NPC.
 */
class FycieChompyBirdDialogue(val quest: Quest) : DialogueFile() {
  override fun handle(componentId: Int, buttonId: Int) {
    when (quest.getStage(player)) {
      in 0 until 70 -> npcl(FacialExpression.OLD_NORMAL, "You's better talk to Dad, We not talk to wierdly 'umans.").also { stage = END_DIALOGUE }
      in 70 until 90 -> handleIngredientDialogue(player, buttonId)
    }
  }

  private fun handleIngredientDialogue(player: Player?, buttonId: Int) {
    val fycieIngredient = getAttribute(player!!, ChompyBird.ATTR_ING_FYCIE, -1)
    when(stage) {
      0 -> npcl(FacialExpression.OLD_NORMAL, "Dad say's you's roasting da chompy for us! Slurp! Me's wants ${getItemName(fycieIngredient)} wiv mine! Yummy can't wait to eats it.").also { stage = END_DIALOGUE }
    }
    setAttribute(player!!, ChompyBird.ATTR_FYCIE_ASKED, true)
  }
}