package content.quest.member.enlightenedjourney.dialogue

import config.NPCs
import core.api.finishQuest
import core.game.dialogue.DialogueFile
import core.game.dialogue.FacialExpression
import core.game.node.entity.npc.NPC

/**
 * Represents the Auguste dialogue file used for Enlightened Journey quest.
 */
class AugusteDialogueFile() : DialogueFile() {
    override fun handle(componentID: Int, buttonID: Int) {
        npc = NPC(NPCs.AUGUSTE_5049)
        when (stage) {
            0 -> playerl(FacialExpression.FRIENDLY, "So what are you going to do now?").also { stage++ }
            1 -> npcl(FacialExpression.FRIENDLY, "I am considering starting a balloon enterprise. People all over Runescape will be able to travel in a new, exciting way.").also { stage++ }
            2 -> npcl(FacialExpression.FRIENDLY, "As my first assistant, you will always be welcome to use a balloon. You'll have to bring your own fuel, though.").also { stage++ }
            3 -> playerl(FacialExpression.FRIENDLY, "Thanks!").also { stage++ }
            4 -> npcl(FacialExpression.FRIENDLY, "I will base my operations in Entrana. If you'd like to travel to new places, come see me there.").also { stage++ }
            5 -> {
                end()
                finishQuest(player!!, "Enlightened Journey")
            }
        }
    }
}